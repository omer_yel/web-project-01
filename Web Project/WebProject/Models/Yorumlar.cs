namespace WebProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Yorumlar")]
    public partial class Yorumlar
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public int YazarID { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Yorum { get; set; }

        [Column(TypeName = "date")]
        public DateTime YorumTarihi { get; set; }

        public int VideoID { get; set; }

        public virtual Kullanicilar Kullanicilar { get; set; }

        public virtual Videolar Videolar { get; set; }
    }
}
