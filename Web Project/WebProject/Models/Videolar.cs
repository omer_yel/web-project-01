namespace WebProject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Videolar")]
    public partial class Videolar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Videolar()
        {
            Yorumlar = new HashSet<Yorumlar>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Baslik { get; set; }

        public int VideoYukleyen { get; set; }

        public TimeSpan VideoSuresi { get; set; }

        public int IzlenmeSayisi { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime VideoTarihi { get; set; }

        [Required]
        public string VideoYolu { get; set; }

        public string VideoThumbnail { get; set; }



        public virtual Kullanicilar Kullanicilar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Yorumlar> Yorumlar { get; set; }
    }
}
