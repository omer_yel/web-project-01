namespace WebProject
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class dbModel : DbContext
    {
        public dbModel()
            : base("name=dbModel")
        {
        }

        public virtual DbSet<Kullanicilar> Kullanicilar { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<Videolar> Videolar { get; set; }
        public virtual DbSet<Yorumlar> Yorumlar { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Kullanicilar>()
                .HasMany(e => e.Videolar)
                .WithRequired(e => e.Kullanicilar)
                .HasForeignKey(e => e.VideoYukleyen)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kullanicilar>()
                .HasMany(e => e.Yorumlar)
                .WithRequired(e => e.Kullanicilar)
                .HasForeignKey(e => e.YazarID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Videolar>()
                .HasMany(e => e.Yorumlar)
                .WithRequired(e => e.Videolar)
                .HasForeignKey(e => e.VideoID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Yorumlar>()
                .Property(e => e.Yorum)
                .IsUnicode(false);
        }
    }
}
