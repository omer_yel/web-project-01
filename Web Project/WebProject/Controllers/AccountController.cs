﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Text;
using WebProject.Resources;

namespace WebProject.Controllers
{
    public class AccountController : Controller
    {
		dbModel db = new dbModel();
        // GET: Account
        public ActionResult UserPage()
        {
            return View();
        }
		public ActionResult Sil(int id)//Kullanıcıyı Sil
		{
			var users = new List<Kullanicilar>();
			users = db.Kullanicilar.ToList();

			var videos = new List<Videolar>();
			videos = db.Videolar.ToList();

			var comments = new List<Yorumlar>();
			comments = db.Yorumlar.ToList();

			foreach (Kullanicilar item in users)
			{
				if (item.ID == id)
				{
					foreach (Videolar video in videos)
					{
						if (video.VideoYukleyen == id)
						{
							video.VideoYukleyen = -1;
						}
					}
					foreach (Yorumlar yorum in comments)
					{
						if (yorum.YazarID == id)
						{
							yorum.YazarID = -1;
						}
					}
					db.Kullanicilar.Remove(db.Kullanicilar.Find(id));
					db.SaveChanges();
				}
			}
			return Redirect("/Account/UserPage");
		}
		public ActionResult ChangePw(FormCollection form)
		{
			var users = new List<Kullanicilar>();
			users = db.Kullanicilar.ToList();
			var current = Session["Giris"].ToString();

			var eski = form.Get("oldpw");
			var new1 = form.Get("pw1");
			var new2 = form.Get("pw2");

			foreach (Kullanicilar item in users)
			{
				if (item.KullaniciAdi == current)
				{
					if (item.Parola == eski)
					{
						if (new1 == new2)
						{
							item.Parola = new1;
							db.SaveChanges();
							TempData["cpw"] = "<span style='color:green;'>"+Lang.msgChange+".</span>";
						}
						else
						{
							TempData["cpw"] = "<span style='color:red;'>"+Lang.NotSame+"</span>";
						}
						break;
					}
					else TempData["cpw"] = "<span style='color:red;'>"+Lang.CorrectPsswdenter+"</span>";
				}
			}
			return Redirect("/Account/UserPage#s");
		}
		public ActionResult ChangeMail(FormCollection form)
		{
			var users = new List<Kullanicilar>();
			users = db.Kullanicilar.ToList();
			var current = Session["Giris"].ToString();

			var mail = form.Get("mail");
			var sifre = form.Get("pw");

			foreach (Kullanicilar item in users)
			{
				if (item.KullaniciAdi == current)
				{
					if (item.Parola == sifre)
					{
						item.Email = mail;
						db.SaveChanges();
						TempData["cmail"] = "<span style='color:green;'>"+Lang.Changemail+"</span>";
					}
					else
					{
						TempData["cmail"] = "<span style='color:red'>"+Lang.Wrongpswd+"</span>";
					}
					break;
				}
			}

			return Redirect("/Account/UserPage#m");
		}
		public ActionResult DeleteAccount()
		{
			var users = new List<Kullanicilar>();
			users = db.Kullanicilar.ToList();

			var videos = new List<Videolar>();
			videos = db.Videolar.ToList();

			var comms = new List<Yorumlar>();
			comms = db.Yorumlar.ToList();

			var current = Session["Giris"].ToString();
			int id = -1;
			foreach (Kullanicilar item in users)
			{
				if (item.KullaniciAdi == current)
				{
					id = item.ID;
					break;
				}
			}
			if (id != -1)
			{
				foreach (Videolar item in videos)
				{
					if (id == item.VideoYukleyen)
					{
						item.VideoYukleyen = -1;
					}
				}
				foreach (Yorumlar item in comms)
				{
					if (id == item.YazarID) 
					{
						item.YazarID = -1;
					}
				}
				db.Kullanicilar.Remove(db.Kullanicilar.Find(id));
				Session.Remove("Giris");
				if (Request.Cookies["remember"] != null)
				{
					Response.Cookies["remember"].Expires = DateTime.Now.AddDays(-1);
				}
				db.SaveChanges();
				TempData["dAccount"] = "<script type='text/javascript'>alert('"+Lang.DeletedAcct+"');</script>";
			}
			return Redirect("/Home/Index");
		}
		public ActionResult SignOut()
		{
			Session.Remove("Giris");
			Response.Cookies["remember"].Expires = DateTime.Now.AddDays(-1);
			Response.Redirect("/Home/Index");
			return View();
		}
		string encode(string value)
		{
			string sifrelimetin;
			byte[] deger = ASCIIEncoding.ASCII.GetBytes(value);
			sifrelimetin = Convert.ToBase64String(deger);
			sifrelimetin += "?EufeqwDQx";
			return sifrelimetin;
		}
		public ActionResult Login(FormCollection form)
		{
			var users = new List<Kullanicilar>();
			users = db.Kullanicilar.ToList();
			var text = form.Get("submit");
			if (text == "Giriş Yap") 
			{
				var username = form.Get("username");
				var password = form.Get("password");
				var remember = form.Get("checkbox");

				foreach (Kullanicilar item in users)
				{
					if (item.KullaniciAdi.ToLower() == username.ToLower() && item.Parola == password)
					{
						Session.Add("Giris", item.KullaniciAdi);
						if (remember == "on")
						{
							HttpCookie cookie = new HttpCookie("remember");
							cookie.Value = encode(Session["Giris"].ToString());
							cookie.Expires = DateTime.Now.AddYears(30);
							Response.Cookies.Add(cookie);
						}
						break;
					}
				}
				TempData["login"] = "<span style='color:red;'>"+Lang.wrongPswdUser+"</span>";
			}
			if (text == "Kayıt Ol") 
			{
				var newUser = new Kullanicilar();

				var username = form.Get("username");
				var mail = form.Get("email");
				var pw1 = form.Get("password");
				var pw2 = form.Get("pwagain");
				if (pw1 == pw2)
				{
					bool hata = false;
					foreach (Kullanicilar item in users)
					{
						if (item.KullaniciAdi.ToLower() == username.ToLower()) 
						{
							TempData["login"] = "<span style='color:red'>"+Lang.RegisteredUser+"</span>";
							hata = true; break;
						}
						if (item.Email.ToLower() == mail.ToLower())
						{
							TempData["login"] = "<span style='color:red;'>"+Lang.SendPswd+"</span>";
							hata = true; break;
						}
					}
					if (hata) return View();

					newUser.KullaniciAdi = username;
					newUser.Email = mail;
					newUser.Parola = pw1;
					newUser.isAdmin = false;

					int id = db.Kullanicilar.Max(u => u.ID);
					newUser.ID = id + 1;
					db.Kullanicilar.Add(newUser);
					db.SaveChanges();
					TempData["login"]= "<span style='color:green;' >"+Lang.SuccessfullRegister+"</span>";
				}
				else
				{
					TempData["login"] = "<span style='color:red;'>"+Lang.NotSame+"</span>";
				}
			}
			return View();
		}
		public ActionResult HesapKurtarma(FormCollection form)
		{
			Kullanicilar kullanicilar = new Kullanicilar();
			string yeniSifre = "";
			Random random = new Random();
			for (int i = 0; i < 8; i++)
			{
				char harf;
				int rakam;

				int secim = random.Next(0, 2);
				switch (secim)
				{
					case 0:
						rakam = random.Next(0, 10);
						yeniSifre += rakam;
						break;
					case 1:
						harf = (char)random.Next(65, 91);
						yeniSifre += harf;
						break;
					default:
						break;
				}
			}


			string icerik = form.Get("mailAdres");
			var emails = new List<Kullanicilar>();
			emails = db.Kullanicilar.ToList();
			if (icerik != null)
			{
				foreach (Kullanicilar item in emails)
				{
					if (icerik == item.Email)
					{
						MailMessage ePosta = new MailMessage();
						string myP = "noreplyvideovision@gmail.com";
						ePosta.From = new MailAddress(myP);
						ePosta.To.Add(item.Email);
						ePosta.Subject = "Şifre Kurtarma";

						ePosta.IsBodyHtml = true;
						ePosta.Body = "Yeni şifreniz:" + yeniSifre;

						SmtpClient smtp = new SmtpClient();
						smtp.UseDefaultCredentials = false;
						smtp.Credentials = new System.Net.NetworkCredential(myP, "videovisio");
						smtp.Port = 587;
						smtp.Host = "smtp.gmail.com";
						smtp.EnableSsl = true;
						bool kontrol = false;
						string hata = "";
						try
						{
							smtp.Send(ePosta);
						}
						catch (SmtpException ex)
						{
							kontrol = true;
							hata = ex.Message;
						}
						if (!kontrol)//Mail Yollandıysa
						{
							item.Parola = yeniSifre;
							db.SaveChanges();
							TempData["mailMsg"] = "<script type='text/javascript'>alert('"+Lang.SendPswd+"');</script>";
							break;
						}
						else
						{
							TempData["mailMsg"] = "<script type='text/javascript'>alert('" + hata + "');</script>";
						}

					}
					else
					{
						TempData["mailMsg"] = null;
					}
				}
			}
			if (TempData["mailMsg"] == null && icerik != null) 
			{
				TempData["mailMsg"] = "<script type='text/javascript'>alert('"+Lang.ValidMailaddress+"');</script>";
			}
			return View();
		}
	}
}