﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebProject.Controllers
{
    public class YorumController : Controller
    {
		dbModel db = new dbModel();
        // GET: Yorum
        public ActionResult Index()
        {
            return View();
        }
		public ActionResult Sil(int yorumId)
		{
			var comms = new List<Yorumlar>();
			comms = db.Yorumlar.ToList();

			foreach (Yorumlar item in comms)
			{
				if (yorumId == item.ID) 
				{
					db.Yorumlar.Remove(item);
				}
			}
			db.SaveChanges();
			return Redirect("/Account/UserPage");
		}
		public ActionResult NormalSil(int yorumId,int videoId)
		{
			var comms = new List<Yorumlar>();
			comms = db.Yorumlar.ToList();

			foreach (Yorumlar item in comms)
			{
				if (yorumId == item.ID)
				{
					db.Yorumlar.Remove(item);
				}
			}
			db.SaveChanges();
			return RedirectToAction("Playing","Videos",new { videoId = videoId });
		}
	}
}