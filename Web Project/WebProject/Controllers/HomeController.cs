﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace WebProject.Controllers
{
    public class HomeController : Controller
    {
		dbModel db = new dbModel();
		// GET: Home
		public ActionResult Index()
		{
            var videos = new List<Videolar>();
            videos = db.Videolar.ToList();
            var secili = db.Videolar.OrderByDescending(x=>x.VideoTarihi).Take(9); 
            return View(videos);
        }
        public ActionResult Change(string langAbbr)
        {
            if(langAbbr!= null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(langAbbr);
                Thread.CurrentThread.CurrentUICulture=CultureInfo.CreateSpecificCulture(langAbbr);
            }

            HttpCookie cookie = new HttpCookie("language");
            cookie.Value = langAbbr;
            Response.Cookies.Add(cookie);

            return View("Index");
        }
    }
}