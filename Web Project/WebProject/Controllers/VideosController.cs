﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProject.Controllers;
using System.IO;
using System.Drawing;
using NReco.VideoInfo;
using NReco.VideoConverter;
//using Accord.Video.FFMPEG;

namespace WebProject.Controllers
{
    public class VideosController : Controller
    {
        dbModel db = new dbModel();


        // GET: Videos

        public ActionResult Sil(int videoId)
        {
            var videos = new List<Videolar>();
            videos = db.Videolar.ToList();

            var comms = new List<Yorumlar>();
            comms = db.Yorumlar.ToList();

            foreach (Videolar item in videos)
            {
                if (videoId == item.ID)
                {
                    foreach (Yorumlar com in comms)
                    {
                        if (com.VideoID == item.ID)
                        {
                            db.Yorumlar.Remove(db.Yorumlar.Find(com.ID));
                        }
                    }
                    FileInfo myfile = new FileInfo(Path.Combine(Server.MapPath("~/requiredFiles/videos/"), item.VideoYolu.Replace("/requiredFiles/videos/", string.Empty)));
                    db.Videolar.Remove(db.Videolar.Find(item.ID));
                    myfile.Delete();
                }
            }
            db.SaveChanges();
            return Redirect("/Account/UserPage");
        }

        public ActionResult Playing(int videoId)
        {
            Videolar video = new Videolar();
            video.ID = videoId;
            db.Videolar.Find(videoId).IzlenmeSayisi++;
            db.SaveChanges();
            return View(video);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Playing(FormCollection form, int videoId)
        {
            var vList = new List<Videolar>();
            var yList = new List<Yorumlar>();
            var yor = new Yorumlar();
            vList = db.Videolar.ToList();
            yList = db.Yorumlar.ToList();

            var users = new List<Kullanicilar>();
            users = db.Kullanicilar.ToList();
            int id = -1;
            foreach (Kullanicilar item in users)
            {
                if (Session["Giris"].ToString() == item.KullaniciAdi)
                {
                    id = item.ID;
                    break;
                }
            }

            yor.ID = db.Yorumlar.Max(x => x.ID) + 1;
            yor.YazarID = id;
            yor.VideoID = videoId;
            yor.YorumTarihi = DateTime.Now;
            yor.Yorum = form.Get("yorum");

            if (ModelState.IsValid)
            {
                db.Yorumlar.Add(yor);
                db.SaveChanges();
                return RedirectToAction("Playing", new { videoId = videoId });
            }

            return RedirectToAction("Playing", new { videoId = videoId });
        }

        [HttpGet]
        public ActionResult UploadPage()
        {
            return View();
        }



        [HttpPost]
        public ActionResult UploadPage(FormCollection form, HttpPostedFileBase dosya, HttpPostedFileBase resim)
        {
            Videolar videoss = new Videolar();
            //VideoFileReader reader = new VideoFileReader();


            var users = new List<Kullanicilar>();
            users = db.Kullanicilar.ToList();
            int id = -1;
            foreach (Kullanicilar item in users)
            {
                if (item.KullaniciAdi == Session["Giris"].ToString())
                {
                    id = item.ID;
                }
            }
            if (ModelState.IsValid)
            {
                videoss.ID = db.Videolar.Max(x => x.ID) + 1;
                videoss.VideoTarihi = DateTime.Now;
                videoss.IzlenmeSayisi = 0;
                videoss.VideoYukleyen = id;
                videoss.Baslik = form.Get("baslik");
                //Path.Combine(Server.MapPath("~\requiredFiles\videos"), fileName);
                var fileName = "";
                if (dosya != null)
                {
                    fileName = dosya.FileName;
                    var path = "/requiredFiles/videos/" + fileName;
                    videoss.VideoYolu = path;

                    dosya.SaveAs(Path.Combine(Server.MapPath("~/requiredFiles/videos/"), fileName));

                    var ffProbe = new FFProbe();
                    var videoInfo = ffProbe.GetMediaInfo(Path.Combine(Server.MapPath("~/requiredFiles/videos/"), fileName));

                    videoss.VideoSuresi = videoInfo.Duration;
                    db.Videolar.Add(videoss);
                    db.SaveChanges();
                    if (resim != null)
                    {
                        var resimname = resim.FileName;
                        resim.SaveAs(Path.Combine(Server.MapPath("~/requiredFiles/capturedFrames/"), resimname));
                        var pathr = "/requiredFiles/capturedFrames/" + resimname;
                        videoss.VideoThumbnail = pathr;
                    }
                    else
                    {
                        var pathResim = "~/requiredFiles/capturedFrames/";
                        //string filename = Path.GetFileName(path);
                        //double Framerate = reader.FrameRate.Value;
                        //int intFrameRate = Convert.ToInt32(Framerate);

                        //for (int i = 0; i <= intFrameRate * 12 /*frameSecondHalf*/; i++)
                        //{
                        //    Bitmap videoFrame = reader.ReadVideoFrame();
                        //    //if (i == (intFrameRate * frameSecondHalf))
                        //    if (i == (intFrameRate * 10))
                        //    {


                        //        videoFrame.Save(i + ".png");

                        //        string capturedframeName = i.ToString() + ".png";
                        //        if (!System.IO.File.Exists(@"~/requiredFiles/capturedFrames/" + filename + "_" + capturedframeName))
                        //        {

                        //            System.IO.File.Move(@"C:\Users\EnýY\source\repos\getFrame01\getFrame01\bin\Debug\" + capturedframeName, @"~/requiredFiles/capturedFrames/" + filename + "_" + capturedframeName);
                        //            videoss.VideoThumbnail = "~/requiredFiles/capturedFrames/" + filename + "_" + capturedframeName;
                        //        }
                        //    }
                        //}
                        var ffmpeg = new FFMpegConverter();
                        ffmpeg.GetVideoThumbnail(path, pathResim, 30);
                    }


                    db.Videolar.Add(videoss);
                    db.SaveChanges();
                    return Redirect("/Home/Index");
                }
                else
                {

                }
            }

            return RedirectToAction("UploadPage");
        }
    }
}