﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebProject.Controllers
{
    public class SharedController : Controller
    {
		// GET: Shared
		public ActionResult Search(string arama)
		{ 
			Models.Arama ara = new Models.Arama();
			ara.aranilan = arama;
			return View(ara);
		}
    }
}