﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace WebProject.Controllers
{
    public class LanguageController : Controller
    {
        // GET: Language
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Change(string langAbbr)
        {
            if (langAbbr != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(langAbbr);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(langAbbr);
            }

            HttpCookie cooki = new HttpCookie("language");
            cooki.Value = langAbbr;
            Response.Cookies.Add(cooki);

            return Redirect(Request.UrlReferrer.PathAndQuery);
        }
    }
}